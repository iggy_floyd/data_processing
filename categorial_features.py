''' An example how to transform the cateogircal variables to numeric representation.

    More details can be found here
    http://scikit-learn.org/stable/modules/preprocessing.html#encoding-categorical-features
'''

from sklearn import preprocessing
import sys

### Excersise 1
### suppose that all Fraud routes were:
data1 = ['FR-FR','FR-DE','FR-FR','DE-BY','BY-DE','FR-FR','DE-DE','DE-FR']
try:
 lb = preprocessing.LabelBinarizer()
 lb.fit(data1)
 print 'classes ', lb.classes_

 ### new data on Frauds was come
 data1_new = ['BY-BY','FR-BY','DE-FR']
 print lb.transform(data1_new)
except:
 print 'Excersise 1 is failed',sys.exc_info()[0]

### Excersise 2
### suppose that all Fraud routes were:
data2 = ['FR-FR','FR-DE','FR-FR','DE-BY','BY-DE','FR-FR','DE-DE','DE-FR']

try:
 le=preprocessing.LabelEncoder()
 le.fit(data2)
 print 'classes ', le.classes_
 ### new data on Frauds was come
 data2_new = ['FR-BY','DE-FR']
 print le.transform(data2_new)
except Exception as e:
 print 'Excersise 2 is failed',sys.exc_info()[0],"Error({0})".format(e)



