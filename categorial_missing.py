''' An example how to deal with missing data from the cateogircal variables.

    More details can be found here
    http://scikit-learn.org/stable/modules/preprocessing.html#imputation-of-missing-values
'''

from sklearn import preprocessing
from sklearn.preprocessing import Imputer

import sys
import numpy as np

def getClassLabel(le,label):
 ''' return the index of the class for the Label created in LabelEncoder'''
 lemap = [(x,i) for i,x in enumerate(tuple(le.classes_))]
 
 return dict(lemap)[label]


### Excersise 1
### suppose that all Fraud routes and Fraud markets were:
data1 = ['FR-FR','FR-DE','FR-FR','DE-BY','BY-DE','FR-FR','DE-DE','DE-FR']
data2 = ['FR','DE','FR','BY','NaN','DE','FR','DE','DE'] # where  NaN was unknow types of the market codes

try:
 le1=preprocessing.LabelEncoder()
 le2=preprocessing.LabelEncoder()
 le1.fit(data1)
 le2.fit(data2)
 print '(1) classes ', le1.classes_
 print '*'*60,'\n'
 print '(2) classes ', le2.classes_
 print '*'*60,'\n'


 ### new data on Frauds had been come ...
 data1_new = ['FR-FR','DE-FR','DE-DE']
 data2_new = ['FR','DE','NaN']

 ### check that there is a correspondence
 print 'new routes of the frauds ', data1_new
 print '*'*60,'\n'
 print 'new markets of the frauds ', data2_new
 print '**'*60,'\n'
 print 'new routes of the frauds in numerical representation ', le1.transform(data1_new)
 print '**'*60,'\n'
 print 'new markets of the frauds in numerical represenation ', le2.transform(data2_new)
 print '**'*60,'\n'

 #transform, merge data and replace all 'NaN' with np.nan
 data2_new = list(le2.transform(data2_new))
 data2_new = map(lambda x: np.nan if x == getClassLabel(le2,'NaN') else x, data2_new)
 data_merge_new = zip(le1.transform(data1_new),data2_new)

 print 'merged new fraud data in numerical representation ', data_merge_new
 print '**'*60,'\n'


 # learn Imputer to imput missing data
 # first, prepare training data 
 data_merge = zip(le1.transform(data1), map(lambda x: np.nan if x == getClassLabel(le2,'NaN') else x,  list(le2.transform(data2))))

 # then, train the impurter
 imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
 imp.fit(data_merge) 


 #finally, apply impurter to the new comming data
 print 'application imputer to replace "nan" data', list(imp.transform(data_merge_new))
 print '**'*60,'\n'

# print le.transform(data2_new)
except Exception as e:
 print 'Excersise 2 is failed',sys.exc_info()[0],"Error({0})".format(e)



