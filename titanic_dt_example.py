''' This is an example of the appication of the Decision Trees on the data ( categorial as well ) stored in the csv 
    To deal with categorial data and missing data     sklearn.preprocessing.LabelEncoder and sklearn.preprocessing.Imputer
    are used.

    This example was taken from 
    http://www.markhneedham.com/blog/2013/11/09/python-making-scikit-learn-and-pandas-play-nice/

    and 

    https://github.com/mneedham/kaggle-titanic

    DataFrameImputer was taken from 
    http://stackoverflow.com/questions/25239958/impute-categorical-missing-values-in-scikit-learn

'''

import pandas as pd
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import sys
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO
import prettytable    



class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)




def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]   
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders 


# 1) get data for training 
train_df = pd.read_csv("train.csv")

# 2) define what input variables (among PassengerId,Survived,Pclass,Name,Sex,Age,SibSp,Parch,Ticket,Fare,Cabin,Embarked) will be used for trainig
# let's say, they are Pclass,Sex,Age,Fare,Embarked
input_variable_names = ['Pclass','Sex','Age','Fare','Embarked']

# 3) we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
print '**'*60,'\n'
print input_variable_types

# 4) do variable transformation
encoders = transform_data(train_df,input_variable_names)

# 5) get classification labels
labels = train_df["Survived"].values

# 6)  DataFrameImputer to fix np.nan
train_df = DataFrameImputer().fit( train_df).transform(train_df)


#print train_df['Age'].values

# 6) get features to be used for training 
features1 = train_df[list(input_variable_names[0:2])].values
features2 = train_df[list(input_variable_names[0:3])].values
features3 = train_df[list(input_variable_names[0:4])].values
features4 = train_df[list(input_variable_names[0:5])].values

# 7) train different features

et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
et_score1 = cross_val_score(et, features1, labels, n_jobs=-1).mean()
et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
et_score2 = cross_val_score(et, features2, labels, n_jobs=-1).mean()
et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
et_score3 = cross_val_score(et, features3, labels, n_jobs=-1).mean()
et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
et_score4 = cross_val_score(et, features4, labels, n_jobs=-1).mean()


# 8) check accuracy of trainigs
print '**'*60,'\n'
print("{0} -> ET: {1})".format(input_variable_names[0:2], et_score1))
print '**'*60,'\n'
print("{0} -> ET: {1})".format(input_variable_names[0:3], et_score2))
print '**'*60,'\n'
print("{0} -> ET: {1})".format(input_variable_names[0:4], et_score3))
print '**'*60,'\n'
print("{0} -> ET: {1})".format(input_variable_names[0:5], et_score4))


# 9) do prediction of test dataset  with et on input_variable_names[0:2]

## first, prepare test data
test_df = pd.read_csv("test.csv")
test_df = DataFrameImputer().fit( test_df).transform(test_df)
encoders = transform_data(test_df,input_variable_names)

## second, train Decision Tree
features = train_df[list(input_variable_names[0:2])].values
et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
et.fit(features,labels)

## third, do prediction
print '**'*60,'\n'
print et.predict(test_df[list(input_variable_names[0:2])].values)

# 10 ) save and print  the prediction for each test passanger
predictions = et.predict(test_df[list(input_variable_names[0:2])].values)
test_predict = pd.DataFrame()
test_predict["Name"] =  pd.Series(test_df["Name"].values)
test_predict["Survived"] =  pd.Series(predictions)

# to print as a table, see: http://stackoverflow.com/questions/18528533/pretty-print-pandas-dataframe
output = StringIO()
test_predict.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print pt


# 11) Print predictions for training data
test_predict = pd.DataFrame()
predictions = et.predict(train_df[list(input_variable_names[0:2])].values)
test_predict["Name"] =  pd.Series(train_df["Name"].values)
test_predict["Survived True"] =  pd.Series(train_df["Survived"].values)
test_predict["Survived from Prediction"] =  pd.Series(predictions)
# to print as a table, see: http://stackoverflow.com/questions/18528533/pretty-print-pandas-dataframe
output = StringIO()
test_predict.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print pt




