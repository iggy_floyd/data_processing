''' 
This is an example of the outlier detector proposed by Ke Wang etc in "One-Class Training for Masquerade Detection" paper
http://www.cs.columbia.edu/~kewang/paper/DMSEC-camera.pdf


 It uses Naive Bayes with Bernoulli Likelihood for two-class problem.

'''




from libsvm.python import svm,svmutil

import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO

import pylab as pl
import matplotlib.font_manager

from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
import sys
import numpy as np
from sklearn import svm

from sklearn import preprocessing
from sklearn.naive_bayes import BernoulliNB,MultinomialNB


from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve, auc,precision_recall_curve


class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)


def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders


# 1) get data for an analysis
df = pd.read_csv("train.csv")

# 2) we are going to analyze a regressiong factors between 'Survied' as a target and Pclass,Sex,Age,Fare,Embarked
input_variable_names = ['Pclass','Sex','Embarked','Age','Fare']


# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

cols_to_keep = ['Survived','Pclass','Sex','Embarked','Age','Fare']
train_cols = ['Pclass','Sex','Embarked','Age','Fare']
data = df[cols_to_keep]




# 3) select one class: 'Survived = 1'
## and get train and test samples 
#data_survived = data.loc[data['Survived']>0]
data_survived = data
X_train, X_test, y_train, y_test = train_test_split(data_survived[train_cols],data_survived['Survived'] , test_size=0.5, random_state=0)


## 4) transform numpy arrays to lists
#data_survived_train = data.loc[data['Survived']>0]
#data_survived_train_pseudo = data_survived_train.copy()
#data_survived_train_pseudo['Survived'] = 0
#data_survived_train=pd.concat([data_survived_train_pseudo,data_survived_train])

data_survived_train = data
data_values = data_survived_train['Survived'].as_matrix()
data_params = data_survived_train[cols_to_keep[1:]].as_matrix()


data_params=map(lambda x: list(x), data_params)



# 5)  create two-class NaiveBaeys Bernoulli  model and train it

# fit the model
clf = MultinomialNB()
clf.fit(data_params,data_values)


# 6) test and evaluate the model 
y_true, y_pred = y_test, clf.predict(X_test)

print(classification_report(y_true, y_pred))


# 9) Compute ROC curve and area under the curve
probas_ = clf.predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])


roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)' % (roc_auc))
pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
pl.xlim([-0.05, 1.05])
pl.ylim([-0.05, 1.05])
pl.xlabel('False Positive Rate (FPR or Type-1 error )')
pl.ylabel('True Positive Rate (TPR or recall or Type-2 error)')
pl.title('Receiver operating characteristic example')
pl.legend(loc="lower right")
pl.show()


# 10) Compute Precision (PositivePredictiveValue=TP/(TP+FP))  vs recall (TPR=P/(TP+FN))
# http://scikit-learn.org/stable/auto_examples/plot_precision_recall.html
ppv, tpr, thresholds = precision_recall_curve(y_test, probas_[:, 1])
roc_auc = auc(tpr, ppv)
pl.plot(tpr, ppv, lw=1, label='Precsion-Recall curve (area = %0.2f)' % (roc_auc))
pl.xlim([-0.05, 1.05])
pl.ylim([-0.05, 1.05])
pl.ylabel('PPV (or precision)')
pl.xlabel('True Positive Rate (TPR or recall)')
pl.title('Precsion-Recall Curve example')
pl.legend(loc="lower right")
pl.show()

raw_input("Press the Enter key...")

