'''
This is an example of outlier detection using  Naive Bayes (NB) One-Class Classifier by means of the 'libnb'
library.
This is just an example!  Sklearn package should give NB ne-Class Classifier to be more efficient!

The problem is that 'libnb'  uses Gaussian Likelihood which is not the case for some categorical data.
'''

from libnb import OneClassNB

import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO


class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)



def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders


# 1) get data for an analysis
df = pd.read_csv("train.csv")

# 2) we are going to analyze a regressiong factors between 'Survied' as a target and Pclass,Sex,Age,Fare,Embarked
input_variable_names = ['Pclass','Sex','Embarked','Age','Fare']


# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

cols_to_keep = ['Survived','Pclass','Sex','Embarked','Age','Fare']
train_cols = ['Pclass','Sex','Embarked','Age','Fare']
data = df[cols_to_keep]

# 3) create One-Class NB and train it
nb = OneClassNB.OneClassNB(None, 0,1,1) # first argument after None (0) -- first index to indicate the classification column. 0 -- "train to class 0", 1 -- an extension using log-probabilities
nb.dataset = data.as_matrix()
trainingSet, testSet = nb.splitDataset(0.50)
print('Split {0} rows into train={1} and test={2} rows').format(len(nb.dataset), len(trainingSet), len(testSet))
summary=nb.summarizeByClass()
print('Summary by class value: {0}').format(summary)

# 4) some tests
maxproba=nb.getMaxProbability(True)
minproba=nb.getMaxProbability(False)
print('Max probability: {0}').format(maxproba)  
print('Min probability: {0}').format(minproba)  
test_level = 0.0000000001*maxproba;
accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy(level=0.0000000001*nb.getMaxProbability(True))
print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )

CL=0.6
maxproba,minproba,CI_min,CI_max = nb.getProbabilitiesClass(CL=CL)
print ('Max Proba for the Class: {0}, Min Proba for the Class: {1}, CI_min  for the Class: {2}, CI_max  for the Class: {3}' ).format(maxproba,minproba,CI_min,CI_max)
accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy(level=CI_min)
print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )


