'''
	This is an example of the very robust outlier detection.
	Class OneClassNB_Binomial performs the Naive Baeys classification for one-class problems.
	It uses Binomial likelihood as it is stated in the paper
	 "One-Class Training for Masquerade Detection" http://www.cs.columbia.edu/~kewang/paper/DMSEC-camera.pdf
	Basic ideas were taken from 
	http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/

        To generate HTML documentation for this module issue the       command:

        pydoc -w OneClassNB_Binomial
'''

import csv
import random
import math
import copy

from sklearn import preprocessing


class OneClassNB_Binomial(object):
 '''
      OneClassNB_Binomial -- Naive Baeys classification for one-class problems. It uses Binomial distributions
 '''

 def __init__(self, filename,class_index,class_to_train,extension=-1):
        '''        Construct a new 'OneClassNB_Binomial' object.       '''

        self.filename = filename

        # stores the index in the training dataset where the class classification can be found.
	# usually, it is -1 (the last element in the data vector or 0 -- the first element)
        self.class_index=class_index


        # this a label used to train the classifier only by data labeled this label :-)
        self.class_to_train = class_to_train

        # extension as supposed at the http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
        self.extension = extension
        self.dataset=[]
	self.encoder = None


 def loadCsv(self,filename=None,skipFirst=True):
        ''' load a dataset to the classifier'''

        if (filename is None): filename=self.filename
        lines = csv.reader(open(filename, "rb"))
        dataset = list(lines)

	
	# skip first line, which might be a comment containing the column names in the csv file.
        start = 1 if skipFirst else 0

        for i in range(start,len(dataset)):         
         self.dataset.append([float(x) for x in dataset[i]])
        
        return self.dataset
 

 def encodeDataset(self,encoder=None,dataset=None,n_values="auto"):
	''' performs the encoding of the datasets '''
	if (encoder==None):	self.encoder = preprocessing.OneHotEncoder(n_values=n_values)
	if (dataset==None):     dataset=copy.deepcopy(self.dataset)


        dataset = zip(*dataset)
        del dataset[self.class_index]
        dataset = zip(*dataset)

	#dataset = map(lambda x: x.remove(x[self.class_index]),dataset)
	self.encoder.fit(dataset)

	return self.encoder


 def transform(self, inputVector):
	''' transform the input vector according encoding used '''
	

	if (self.encoder): return self.encoder.transform([inputVector]).toarray().tolist()[0]

	return inputVector

 def transformDataset(self, dataset=None):
        ''' transform the input vector according encoding used '''


        if (dataset==None):     dataset=copy.deepcopy(self.dataset)

        dataset = zip(*dataset)
        del dataset[self.class_index]
        dataset = zip(*dataset)

	#dataset = map(lambda x: x.pop(self.class_index),dataset)

	if (self.encoder==None):     
	 return dataset

        return self.encoder.transform(dataset).toarray().tolist()



 def splitDataset(self,splitRatio):
        ''' split the dataset for training and testing  '''

        trainSize = int(len(self.dataset) * splitRatio)
        self.trainSet = []
        self.testSet = list(self.dataset)
        while len(self.trainSet) < trainSize:
          index = random.randrange(len(self.testSet))
          self.trainSet.append(self.testSet.pop(index))
        
        return [self.trainSet, self.testSet]
 


 def separateByClass(self,dataset):
        ''' returns two datasets, one for each class accordingly '''
        separated = {}
        
        for i in range(len(dataset)):
         vector = dataset[i]
         if (vector[self.class_index] != self.class_to_train ): continue

         if (vector[self.class_index] not in separated):
          separated[vector[self.class_index]] = []
         separated[vector[self.class_index]].append(vector)
        
        return separated


 def bernoulli_prob(self, numbers):
	''' calculate the bernoulli probability for numbers like [0,0,1,0,1,1,1 etc] '''

	_sum = sum(numbers)
	
	# using Laplacean prior
	return (1.0 + float(_sum))/(2.0 + float(len(numbers)))


 def mean(self,numbers):
	''' calculates the mean value of the attribute distribution given by numbers'''

	return sum(numbers)/float(len(numbers))
 

 def stdev(self,numbers):
	''' calculates the std. deviation of the attribute distribution given by numbers'''

	avg = self.mean(numbers)

	# here we use N-1 estimator. It is unbiased estimator. See http://en.wikipedia.org/wiki/Standard_deviation .
	variance = sum([pow(x-avg,2) for x in numbers])/float(len(numbers)-1) 
	return math.sqrt(variance) 

 def summarize(self,dataset):
 	''' calculates the mean value and  std. deviation  for each attribute in the dataset '''

        
	summaries = [(self.mean(attribute), self.stdev(attribute)) for attribute in zip(*dataset)]
	del summaries[self.class_index]

	return summaries
 
 def summarize_bernoulli(self,dataset):
	''' return the bernoulli probabilities for attributes '''
	
        dataset = self.transformDataset(dataset)
	summaries = [self.bernoulli_prob(attribute) for attribute in zip(*dataset)]
        
        return summaries


 def summarizeByClass(self,train=True,dataset=None):
	''' calculates the mean value and  std. deviation  for each attribute in the datasets   
	    separated among classes
        '''

	if (dataset is not None):
  	 separated = self.separateByClass(dataset)
	else:
	 if (train):  separated = self.separateByClass(self.trainSet)
	 else: separated = self.separateByClass(self.testSet)
	
	summaries = {}
	for classValue, instances in separated.iteritems():
	 summaries[classValue] = self.summarize(instances)

	return summaries


 def summarizeByClass_bernoulli(self,train=True,dataset=None):
        ''' calculates the mean value and  std. deviation  for each attribute in the datasets   
            separated among classes
        '''

        if (dataset is not None):
         separated = self.separateByClass(dataset)
        else:
         if (train):  separated = self.separateByClass(self.trainSet)
         else: separated = self.separateByClass(self.testSet)

        summaries = {}
        for classValue, instances in separated.iteritems():
         summaries[classValue] = self.summarize_bernoulli(instances)

        return summaries


 
 def calculateProbability(self,x, mean, stdev):
	''' calculate Likelihood of the attribute '''
	exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
	return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent
 

 def calculateProbability_bernoulli(self,bit, prob):
	''' calculate Likelihood of the attribute '''
  
	return float(bit)*prob + float(1.0 - float(bit))*(1.0-prob)


 def calculateClassProbabilities(self,inputVector,summaries=None):
	''' calculate the class probabilities '''
	probabilities = {}

	if (summaries is None): summaries = self.summarizeByClass()

        if (self.extension < 1):

	 for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 1
	  for i in range(len(classSummaries)):
	   mean, stdev = classSummaries[i]
	   x = inputVector[i]
	   probabilities[classValue] *= self.calculateProbability(x, mean, stdev)

        elif (self.extension == 1): ## log-probabilities
         for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 0
          for i in range(len(classSummaries)):
	   mean, stdev = classSummaries[i]
	   x = inputVector[i]
           probabilities[classValue] += math.log(self.calculateProbability(x, mean, stdev))

        
	return probabilities


 def calculateClassProbabilities_bernoulli(self,inputVector,summaries=None):
	''' calculate the class probabilities '''
	probabilities = {}

	if (summaries is None): summaries = self.summarizeByClass_bernoulli()

        if (self.extension < 1):

	 for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 1
	  for i in range(len(classSummaries)):
	   proba= classSummaries[i]
	   x = inputVector[i]
	   probabilities[classValue] *= self.calculateProbability_bernoulli(x, proba)

        elif (self.extension == 1): ## log-probabilities
         for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 0
          for i in range(len(classSummaries)):
	   proba = classSummaries[i]
	   x = inputVector[i]
           probabilities[classValue] += math.log(self.calculateProbability_bernoulli(x,proba))

        
	return probabilities



 def predict(self,inputVector,summaries = None):
	''' predict the the class and the probability'''

	if (summaries is None): summaries = self.summarizeByClass()

	probabilities = self.calculateClassProbabilities(inputVector,summaries)

	bestLabel, bestProb = None, -1

	for classValue, probability in probabilities.iteritems():
	 if bestLabel is None or probability > bestProb:
	  bestProb = probability
	  bestLabel = classValue

	return (bestLabel,bestProb)
 


 def predict_bernoulli(self,inputVector,summaries = None):
	''' predict the the class and the probability'''

	if (summaries is None): summaries = self.summarizeByClass_bernoulli()


        inputVector_transformed = copy.deepcopy(inputVector)
        del inputVector_transformed[self.class_index]
        inputVector_transformed = self.transform(inputVector_transformed)

	probabilities = self.calculateClassProbabilities_bernoulli(inputVector_transformed,summaries)

	bestLabel, bestProb = None, -1

	for classValue, probability in probabilities.iteritems():
	 if bestLabel is None or probability > bestProb:
	  bestProb = probability
	  bestLabel = classValue

	return (bestLabel,bestProb) 



 def getPredictions(self, testSet=None,summaries = None):
	''' return predictions on testSet '''

	if (testSet is None): testSet=self.testSet
	if (summaries is None): summaries = self.summarizeByClass()


	predictions = []

	for i in range(len(testSet)):
 	 resultLabel,resultProb = self.predict(testSet[i],summaries)
	 predictions.append([resultLabel,resultProb])

	return predictions


 def getPredictions_bernoulli(self, testSet=None,summaries = None):
	''' return predictions on testSet '''

	if (testSet is None): testSet=self.testSet
	if (summaries is None): summaries = self.summarizeByClass_bernoulli()


	predictions = []

	for i in range(len(testSet)):
 	 resultLabel,resultProb = self.predict_bernoulli(testSet[i],summaries)
	 predictions.append([resultLabel,resultProb])

	return predictions



 def getMaxProbability(self,_max=True,testSet=None,summaries = None):
        ''' return the max probability value in testSet'''

	if (testSet is None): testSet=self.testSet
	if (summaries is None): summaries = self.summarizeByClass()

        predictions = self.getPredictions(testSet,summaries)
        if (_max): return max(map(lambda x: x[1],predictions))
        else: return min(map(lambda x: x[1],predictions))

 def getMaxProbability_bernoulli(self,_max=True,testSet=None,summaries = None):
        ''' return the max probability value in testSet'''

	if (testSet is None): testSet=self.testSet
	if (summaries is None): summaries = self.summarizeByClass_bernoulli()

        predictions = self.getPredictions_bernoulli(testSet,summaries)
        if (_max): return max(map(lambda x: x[1],predictions))
        else: return min(map(lambda x: x[1],predictions))




 def getProbabilitiesClass(self,CI_type='double',CL=0.025,testSet=None,summaries = None):
        ''' return max and min probabilities for the Class, and CI ('upper','lower', 'double') '''

        if (testSet is None): testSet=self.testSet
        if (summaries is None): summaries = self.summarizeByClass()

        testSet = filter(lambda x: x[self.class_index] == self.class_to_train,testSet)


        predictions = self.getPredictions(testSet,summaries)   
        predictions_probas = map(lambda x: x[1],predictions)

        maxproba=max(predictions_probas)
        minproba=min(predictions_probas)
        

        # calculation of CI
        # idea taken from
        # http://stackoverflow.com/questions/21453404/plotting-confidence-intervals-for-maximum-likelihood-estimate
        # http://stackoverflow.com/questions/21532667/numpy-histogram-cumulative-density-does-not-sum-to-1

        try:
            import numpy as np
        except ImportError:
            return(maxproba,minproba,None,None)

#        print predictions_probas
        #density, bins = np.histogram(predictions_probas, normed=True, density=True)
        density, bins = np.histogram(predictions_probas,bins=100,density=True)
        

#        print np.cumsum(predictions_probas)
#        print np.cumsum(predictions_probas)/sum(predictions_probas)
#        print density       
#        print bins
        unity_density = density / density.sum()
#        print unity_density

        cdf = unity_density.cumsum()
        CI_min = None
        CI_max = None

        #print cdf

        # get the left and right boundary of the interval that contains 95% of the probability mass 
        right=np.argmax(cdf>1.-CL)
        left=np.argmax(cdf>CL)
        CI_min = bins[left+1]
        CI_max = bins[right+1]


        if (CI_type=='double'):
            return (maxproba,minproba,CI_min,CI_max)        
        elif (CI_type == 'upper'):
            return (maxproba,minproba,None,CI_max)        

        elif (CI_type == 'lower'):
            return (maxproba,minproba,CI_min,None)        

 def getProbabilitiesClass_bernoulli(self,CI_type='double',CL=0.025,testSet=None,summaries = None):
        ''' return max and min probabilities for the Class, and CI ('upper','lower', 'double') '''

        if (testSet is None): testSet=self.testSet
        if (summaries is None): summaries = self.summarizeByClass_bernoulli()

        testSet = filter(lambda x: x[self.class_index] == self.class_to_train,testSet)


        predictions = self.getPredictions_bernoulli(testSet,summaries)   
        predictions_probas = map(lambda x: x[1],predictions)

        maxproba=max(predictions_probas)
        minproba=min(predictions_probas)
        

        # calculation of CI
        # idea taken from
        # http://stackoverflow.com/questions/21453404/plotting-confidence-intervals-for-maximum-likelihood-estimate
        # http://stackoverflow.com/questions/21532667/numpy-histogram-cumulative-density-does-not-sum-to-1

        try:
            import numpy as np
        except ImportError:
            return(maxproba,minproba,None,None)

#        print predictions_probas
        #density, bins = np.histogram(predictions_probas, normed=True, density=True)
        density, bins = np.histogram(predictions_probas,bins=100,density=True)
        

#        print np.cumsum(predictions_probas)
#        print np.cumsum(predictions_probas)/sum(predictions_probas)
#        print density       
#        print bins
        unity_density = density / density.sum()
#        print unity_density

        cdf = unity_density.cumsum()
        CI_min = None
        CI_max = None

        #print cdf

        # get the left and right boundary of the interval that contains 95% of the probability mass 
        right=np.argmax(cdf>1.-CL)
        left=np.argmax(cdf>CL)
        CI_min = bins[left+1]
        CI_max = bins[right+1]


        if (CI_type=='double'):
            return (maxproba,minproba,CI_min,CI_max)        
        elif (CI_type == 'upper'):
            return (maxproba,minproba,None,CI_max)        

        elif (CI_type == 'lower'):
            return (maxproba,minproba,CI_min,None)        


 def getAccuracy(self,testSet=None, predictions=None,level=0.05):
	'''calculate the precision of selection the 95%CL'''

	if (testSet is None): testSet = self.testSet
	if (predictions is None): predictions=self.getPredictions(testSet)

        #print predictions
         
	correct = 0
        type1_error = 0
        type2_error = 0
        total_accepted = 0
        total_rejected = 0
        total_this_class = 0
        total_other_classes = 0
	for i in range(len(testSet)):

         if testSet[i][self.class_index] != predictions[i][0]:
          total_other_classes +=1
         else:
          total_this_class +=1

         if (predictions[i][1] >  level):
          total_accepted  += 1
          if testSet[i][self.class_index] == predictions[i][0]:
            correct +=1
          else: 
            type2_error +=1
         else:
          total_rejected  += 1
          if testSet[i][self.class_index] == predictions[i][0]:
            type1_error +=1 
         

	return (correct/float(len(testSet)),type1_error/float(len(testSet)),type2_error/float(len(testSet)),
                total_accepted/float(len(testSet)), total_rejected/float(len(testSet)),
                total_this_class/float(len(testSet)), total_other_classes/float(len(testSet)),
                )

 def getAccuracy_bernoulli(self,testSet=None, predictions=None,level=0.05):
	'''calculate the precision of selection the 95%CL'''

	if (testSet is None): testSet = self.testSet
	if (predictions is None): predictions=self.getPredictions_bernoulli(testSet)

        #print predictions
         
	correct = 0
        type1_error = 0
        type2_error = 0
        total_accepted = 0
        total_rejected = 0
        total_this_class = 0
        total_other_classes = 0
	for i in range(len(testSet)):

         if testSet[i][self.class_index] != predictions[i][0]:
          total_other_classes +=1
         else:
          total_this_class +=1

         if (predictions[i][1] >  level):
          total_accepted  += 1
          if testSet[i][self.class_index] == predictions[i][0]:
            correct +=1
          else: 
            type2_error +=1
         else:
          total_rejected  += 1
          if testSet[i][self.class_index] == predictions[i][0]:
            type1_error +=1 
         

	return (correct/float(len(testSet)),type1_error/float(len(testSet)),type2_error/float(len(testSet)),
                total_accepted/float(len(testSet)), total_rejected/float(len(testSet)),
                total_this_class/float(len(testSet)), total_other_classes/float(len(testSet)),
                )


if __name__ == '__main__':

	# Some tests, taken from http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/

	nb = OneClassNB_Binomial(None, -1,0) # want to test the class "0"

        
	# test 1
	dataset = [[1,20,1], [2,21,0], [3,22,1]]
	separated = nb.separateByClass(dataset)
	print('Separated instances: {0}').format(separated)
        
	# test 2
	numbers = [1,2,3,4,5]
	print('Summary of {0}: mean={1}, stdev={2}').format(numbers, nb.mean(numbers), nb.stdev(numbers))

        # test 2.2
        dataset = [[1,20,0], [2,21,1], [3,22,0]]
        nb.encodeDataset(None,dataset)      

        # test 2.3
        transfromed_vector = nb.transform([1,20])
        print('Transformed vector: {0}').format(transfromed_vector )

        # test 2.4
        transfromed_dataset = nb.transformDataset(dataset)
        print('Transformed dataset: {0}').format(transfromed_dataset )

	# test 3
	dataset = [[1,20,0], [2,21,1], [3,22,0]]
	summary = nb.summarize(dataset)
	print('Attribute summaries: {0}').format(summary)

	# test 3
	dataset = [[1,20,0], [2,21,1], [3,22,0],[4,22,0]]
        nb.encodeDataset(None,dataset) 
	summary = nb.summarize_bernoulli(dataset)
	print('Transformed attribute summaries: {0}').format(summary)


        
	# test 4
	dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
	summary = nb.summarizeByClass(None,dataset)
	print('Summary by class value: {0}').format(summary)

        # test 4.2
	dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
	summary = nb.summarizeByClass_bernoulli(None,dataset)
	print('Summary by transformed class value: {0}').format(summary)
        
	# test 5
        x = 71.5
	mean = 73
	stdev = 6.2
	probability = nb.calculateProbability(x, mean, stdev)
	print('Probability of belonging to this class: {0}').format(probability)

	# test 5.2
	dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
        nb.encodeDataset(None,dataset) 
	summary = nb.summarizeByClass_bernoulli(None,dataset)
        transfromed_vector = nb.transform([1,20])
	probability = nb.calculateProbability_bernoulli(transfromed_vector[1],summary[0][1])
	print('Probability of belonging to this transformed class: {0}').format(probability)
        
	# test 6
	summaries = {0:[(1, 0.5)], 1:[(20, 5.0)]}
	inputVector = [1.1, '?']
	probabilities = nb.calculateClassProbabilities(inputVector,summaries)
	print('Probabilities for each class: {0}').format(probabilities)


        # test 6.2
        dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
        nb.encodeDataset(None,dataset)
	summaries =  {0: [0.25, 0.5, 0.25, 0.5, 0.25, 0.5, 0.5],1:[0.45, 0.54, 0.55, 0.47, 0.25, 0.15, 0.51]}
        inputVector = [2, 22]
	inputVector = nb.transform(inputVector)
        probabilities = nb.calculateClassProbabilities_bernoulli(inputVector,summaries)
        print('Probabilities for each transformed class: {0}').format(probabilities)

        
        # test 7
        summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
        inputVector = [1.1, '?']
        result = nb.predict(inputVector,summaries)
        print('Prediction: {0}').format(result)

        # test 7.2
	summaries =  {'A': [0.25, 0.5, 0.25, 0.5, 0.25, 0.5, 0.5],'B':[0.45, 0.54, 0.55, 0.47, 0.25, 0.15, 0.51]}
	inputVector = [2, 22,'?']
        result = nb.predict_bernoulli(inputVector,summaries)
        print('Transformed Prediction: {0}').format(result)

        
        # test 8
        summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
        testSet = [[1.1, '?'], [19.1, '?']]
        predictions = nb.getPredictions(testSet,summaries)
        print('Predictions: {0}').format(predictions)


        # test 8.2
        dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
        nb.encodeDataset(None,dataset,n_values=100)
	summaries =  {'A': [0.25, 0.5, 0.25, 0.5, 0.25, 0.5, 0.5],'B':[0.45, 0.54, 0.55, 0.47, 0.25, 0.15, 0.51]}
        testSet = [[2, 22,'?'],[4,25,'?']]
        predictions = nb.getPredictions_bernoulli(testSet,summaries)
        print('Transformed Predictions: {0}').format(predictions)

        
        # test 9
        testSet = [[1,1,1,'a'], [2,2,2,'a'], [3,3,3,'b']]
        predictions = [['a',1.0], ['a',1.0], ['a',1.0]]
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy(testSet, predictions)
        print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2}' ).format(accuracy,error1,error2,total_accepted,total_rejected )

        # test 9.2
        testSet = [[1,1,1,'a'], [2,2,2,'a'], [3,3,3,'b']]
	nb.encodeDataset(None,dataset,n_values=100)
        predictions = [['a',1.0], ['a',1.0], ['a',1.0]]
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy_bernoulli(testSet, predictions)
        print('Transformed Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2}' ).format(accuracy,error1,error2,total_accepted,total_rejected )


        
        # final test on data https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data
        # description of the attributes
        # https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.names
        # format of aruments: 'a filename with dataset', 'the index of classification','label of the class' ,'some extensions: -1/no exten.,  1/using log-probabilities '
        nb = OneClassNB_Binomial("pima-indians-diabetes.data", -1,0,1)
        
        nb.loadCsv(None,False)
        trainingSet, testSet = nb.splitDataset(0.60)
        print('Split {0} rows into train={1} and test={2} rows').format(len(nb.dataset), len(trainingSet), len(testSet))  
        maxproba=nb.getMaxProbability(True)
        minproba=nb.getMaxProbability(False)

        print('Max probability: {0}').format(maxproba)  
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy(level=0.01*maxproba)
        print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )

        print('Min probability: {0}').format(minproba)  
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes  = nb.getAccuracy(level=0.01*minproba)
        print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )

        CL=0.20
        maxproba,minproba,CI_min,CI_max = nb.getProbabilitiesClass(CL=CL)
        print ('Max Proba for the Class: {0}, Min Proba for the Class: {1}, CI_min  for the Class: {2}, CI_max  for the Class: {3}' ).format(maxproba,minproba,CI_min,CI_max)
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy(level=CI_min)
        print('Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )


	## bernoulli model test
        CL=0.01
	dataset = nb.dataset
	nb.encodeDataset(None,dataset,n_values=1000)
        maxproba,minproba,CI_min,CI_max = nb.getProbabilitiesClass_bernoulli(CL=CL)
        print ('Bernoulli: Max Proba for the Class: {0}, Min Proba for the Class: {1}, CI_min  for the Class: {2}, CI_max  for the Class: {3}' ).format(maxproba,minproba,CI_min,CI_max)
        accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes = nb.getAccuracy_bernoulli(level=CI_min+10)
        print('Bernoulli: Accuracy: {0}, Type-1 error: {1}, Type-2 error: {2},Total Accepted: {3}, Total Rejected: {4},Total This Class: {5},Total Other Classes: {6}' ).format(accuracy,error1,error2,total_accepted,total_rejected,total_this_class,total_other_classes )


 
        # here is a list of data for the class "0" randomly taken from pima-indians-diabetes.data
        data0=[[3,126,88,41,235,39.3,0.704,27],
               [6,92,92,0,0,19.9,0.188,28],
               [7,159,64,0,0,27.4,0.294,40]
              ]
        for item in data0:
            print('Probability of the data to belong to the class "0": {0}').format(nb.predict(item)[1])
        

        # here is a list of data for the class "1" randomly taken from pima-indians-diabetes.data
        data1=[[13,126,90,0,0,43.4,0.583,42],
               [0,131,0,0,0,43.2,0.270,26],
               [0,95,85,25,36,37.4,0.247,24]
              ]
        for item in data1:
            print('Probability of the data to belong to the class "0": {0}').format(nb.predict(item)[1])
