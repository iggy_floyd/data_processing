'''
	This is an example of the very robust outlier detection.
	Class TwoClassNB performs the Naive Baeys classification for two-classes problems.
	Basic ideas were taken from 
	http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/

        To generate HTML documentation for this module issue the       command:

        pydoc -w TwoClassNB
'''

import csv
import random
import math



class TwoClassNB(object):
 '''
      TwoClassNB -- Naive Baeys classification for two-classes problems
 '''

 def __init__(self, filename,class_index,extension=-1):
        '''        Construct a new 'TwoClassNB' object.       '''

        self.filename = filename

        # stores the index in the training dataset where the class classification can be found.
	# usually, it is -1 (the last element in the data vector or 0 -- the first element)
        self.class_index=class_index

        # extension as supposed at the http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
        self.extension = extension
        self.dataset=[]


 def loadCsv(self,filename=None,skipFirst=True):
        ''' load a dataset to the classifier'''

        if (filename is None): filename=self.filename
        lines = csv.reader(open(filename, "rb"))
        dataset = list(lines)

	
	# skip first line, which might be a comment containing the column names in the csv file.
        start = 1 if skipFirst else 0

        for i in range(start,len(dataset)):         
         self.dataset.append([float(x) for x in dataset[i]])
        
        return self.dataset
 


 def splitDataset(self,splitRatio):
        ''' split the dataset for training and testing  '''

        trainSize = int(len(self.dataset) * splitRatio)
        self.trainSet = []
        self.testSet = list(self.dataset)
        while len(self.trainSet) < trainSize:
          index = random.randrange(len(self.testSet))
          self.trainSet.append(self.testSet.pop(index))
        
        return [self.trainSet, self.testSet]
 


 def separateByClass(self,dataset):
        ''' returns two datasets, one for each class accordingly '''
        separated = {}
        
        for i in range(len(dataset)):
         vector = dataset[i]
         if (vector[self.class_index] not in separated):
          separated[vector[self.class_index]] = []
         separated[vector[self.class_index]].append(vector)
        
        return separated



 def mean(self,numbers):
	''' calculates the mean value of the attribute distribution given by numbers'''

	return sum(numbers)/float(len(numbers))
 

 def stdev(self,numbers):
	''' calculates the std. deviation of the attribute distribution given by numbers'''

	avg = self.mean(numbers)

	# here we use N-1 estimator. It is unbiased estimator. See http://en.wikipedia.org/wiki/Standard_deviation .
	variance = sum([pow(x-avg,2) for x in numbers])/float(len(numbers)-1) 
	return math.sqrt(variance)
 

 def summarize(self,dataset):
 	''' calculates the mean value and  std. deviation  for each attribute in the dataset '''

        
	summaries = [(self.mean(attribute), self.stdev(attribute)) for attribute in zip(*dataset)]
	del summaries[self.class_index]

	return summaries
 

 def summarizeByClass(self,train=True,dataset=None):
	''' calculates the mean value and  std. deviation  for each attribute in the datasets   
	    separated among classes
        '''

	if (dataset is not None):
  	 separated = self.separateByClass(dataset)
	else:
	 if (train):  separated = self.separateByClass(self.trainSet)
	 else: separated = self.separateByClass(self.testSet)
	
	summaries = {}
	for classValue, instances in separated.iteritems():
	 summaries[classValue] = self.summarize(instances)

	return summaries


 
 def calculateProbability(self,x, mean, stdev):
	''' calculate Likelihood of the attribute '''
	exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
	return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent
 

 def calculateClassProbabilities(self,inputVector,summaries=None):
	''' calculate the class probabilities '''
	probabilities = {}

	if (summaries is None): summaries = self.summarizeByClass()

        if (self.extension < 1):

	 for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 1
	  for i in range(len(classSummaries)):
	   mean, stdev = classSummaries[i]
	   x = inputVector[i]
	   probabilities[classValue] *= self.calculateProbability(x, mean, stdev)

        elif (self.extension == 1): ## log-probabilities
         for classValue, classSummaries in summaries.iteritems():
	  probabilities[classValue] = 0
          for i in range(len(classSummaries)):
	   mean, stdev = classSummaries[i]
	   x = inputVector[i]
           probabilities[classValue] += math.log(self.calculateProbability(x, mean, stdev))

        # if extension for nomalizing the Likelihhod by P(nputVector)
        if (self.extension == 0):             
            _sum = sum(classProbas  for classValue, classProbas in  probabilities.iteritems() )
            probabilities = { classValue: classProbas/_sum for classValue, classProbas in  probabilities.iteritems() }
        
	return probabilities

 def predict(self,inputVector,summaries = None):
	''' predict the the class and the probability'''

	if (summaries is None): summaries = self.summarizeByClass()

	probabilities = self.calculateClassProbabilities(inputVector,summaries)

	bestLabel, bestProb = None, -1

	for classValue, probability in probabilities.iteritems():
	 if bestLabel is None or probability > bestProb:
	  bestProb = probability
	  bestLabel = classValue

	return (bestLabel,bestProb)
 

 def getPredictions(self, testSet=None,summaries = None):
	''' return predictions on testSet '''

	if (testSet is None): testSet=self.testSet
	if (summaries is None): summaries = self.summarizeByClass()


	predictions = []

	for i in range(len(testSet)):
 	 resultLabel,resultProb = self.predict(testSet[i],summaries)
	 predictions.append((resultLabel,resultProb))

	return predictions
 
 def getAccuracy(self,testSet=None, predictions=None):
	''' calculate the precision of selection'''

	if (testSet is None): testSet = self.testSet
	if (predictions is None): predictions=self.getPredictions(testSet)

	correct = 0
	for i in range(len(testSet)):
	 if testSet[i][self.class_index] == predictions[i][0]:
	  correct += 1

	return (correct/float(len(testSet)))


if __name__ == '__main__':

	# Some tests, taken from http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/

	nb = TwoClassNB(None, -1)

	# test 1
	dataset = [[1,20,1], [2,21,0], [3,22,1]]
	separated = nb.separateByClass(dataset)
	print('Separated instances: {0}').format(separated)

	# test 2
	numbers = [1,2,3,4,5]
	print('Summary of {0}: mean={1}, stdev={2}').format(numbers, nb.mean(numbers), nb.stdev(numbers))

	# test 3
	dataset = [[1,20,0], [2,21,1], [3,22,0]]
	summary = nb.summarize(dataset)
	print('Attribute summaries: {0}').format(summary)

	# test 4
	dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
	summary = nb.summarizeByClass(None,dataset)
	print('Summary by class value: {0}').format(summary)

	# test 5
        x = 71.5
	mean = 73
	stdev = 6.2
	probability = nb.calculateProbability(x, mean, stdev)
	print('Probability of belonging to this class: {0}').format(probability)

	# test 6
	summaries = {0:[(1, 0.5)], 1:[(20, 5.0)]}
	inputVector = [1.1, '?']
	probabilities = nb.calculateClassProbabilities(inputVector,summaries)
	print('Probabilities for each class: {0}').format(probabilities)

        # test 7
        summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
        inputVector = [1.1, '?']
        result = nb.predict(inputVector,summaries)
        print('Prediction: {0}').format(result)

        # test 8
        summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
        testSet = [[1.1, '?'], [19.1, '?']]
        predictions = nb.getPredictions(testSet,summaries)
        print('Predictions: {0}').format(predictions)

        # test 9
        testSet = [[1,1,1,'a'], [2,2,2,'a'], [3,3,3,'b']]
        predictions = ['a', 'a', 'a']
        accuracy = nb.getAccuracy(testSet, predictions)
        print('Accuracy: {0}').format(accuracy)


        # final test on data https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data
        # description of the attributes
        # https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.names
        # format of aruments: 'a filename with dataset', 'the index of classification', 'some extensions: -1/no exten., 0/using marginal P(X), 1/using log-probabilities '
        nb = TwoClassNB("pima-indians-diabetes.data", -1,1)
        nb.loadCsv(None,False)
        trainingSet, testSet = nb.splitDataset(0.60)
        print('Split {0} rows into train={1} and test={2} rows').format(len(nb.dataset), len(trainingSet), len(testSet))
        accuracy=nb.getAccuracy()
        print('Accuracy: {0}').format(accuracy)