''' 
This is an example of the outlier detector proposed by Ke Wang etc in "One-Class Training for Masquerade Detection" paper
http://www.cs.columbia.edu/~kewang/paper/DMSEC-camera.pdf


 svm.OneClassSVM from sklearn will be used instead of libsvm

'''




from libsvm.python import svm,svmutil

import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO

import pylab as pl
import matplotlib.font_manager

from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
import sys
import numpy as np
from sklearn import svm


class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)


def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders


# 1) get data for an analysis
df = pd.read_csv("train.csv")

# 2) we are going to analyze a regressiong factors between 'Survied' as a target and Pclass,Sex,Age,Fare,Embarked
input_variable_names = ['Pclass','Sex','Embarked','Age','Fare']


# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

cols_to_keep = ['Survived','Pclass','Sex','Embarked','Age','Fare']
train_cols = ['Pclass','Sex','Embarked','Age','Fare']
data = df[cols_to_keep]

# 3) select one class: 'Survived = 1'
## and get train and test samples 
data_survived = data.loc[data['Survived']>0]
X_train, X_test, y_train, y_test = train_test_split(data_survived[train_cols],data_survived['Survived'] , test_size=0.5, random_state=0)


## 4) transform numpy arrays to lists
data_survived_train = data.loc[data['Survived']>0]
data_values = data_survived_train['Survived'].as_matrix()
data_params = data_survived_train[cols_to_keep[1:]].as_matrix()
#data_values = list(data_values)
#data_params=list(data_params)

#data_values = list(y_train)
#data_params=list(X_train)

data_params=map(lambda x: list(x), data_params)
#print data_params



# 5)  create one-class SVM model and train it

# fit the model
clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
clf.fit(data_params)




# 6) test and evaluate the model 
y_pred_train = clf.predict(X_train)
y_pred_test = clf.predict(X_test)
n_error_train = y_pred_train[y_pred_train == -1].size
n_error_test = y_pred_test[y_pred_test == -1].size




# 7) plot a test sample: correct and incorrect SVM predicted values
data_params_test_correct = [X_test[i] for i in range(len(X_test))  if  y_pred_test[i] == 1]
data_params_test_incorrect = [X_test[i] for i in range(len(X_test))if   y_pred_test[i] == -1]

## plot Age vs Fare using the following metric
#['Pclass','Sex','Embarked','Age','Fare']
ages_correct=np.array(data_params_test_correct)[:,3]
fares_correct=np.array(data_params_test_correct)[:,4]
ages_incorrect=np.array(data_params_test_incorrect)[:,3]
fares_incorrect=np.array(data_params_test_incorrect)[:,4]
b_correct = pl.scatter(ages_correct,fares_correct, c='green')
b_incorrect = pl.scatter(ages_incorrect,fares_incorrect, c='red')
n_correct = ages_correct.size
n_error = ages_incorrect.size
pl.xlim((ages_correct.min(),ages_correct.max() ))
pl.ylim((fares_correct.min(),fares_correct.max() ))
pl.xlabel('ages')
pl.ylabel('fares')
pl.legend([ b_correct,b_incorrect],
           ["correct (%d)"%n_correct, "incorrect (%d)"%n_error],
           loc="upper left",
           prop=matplotlib.font_manager.FontProperties(size=11))

pl.title("Test on Survived data (%d)"%y_test.size)
pl.show()



# test on non-Survived data
# select one class: 'Survived = 0'
data_nonsurvived = data.loc[data['Survived']==0]
data_values = data_nonsurvived['Survived'].as_matrix()
data_params = data_nonsurvived[cols_to_keep[1:]].as_matrix()
data_values = list(data_values)
data_params=list(data_params)
data_params=map(lambda x: list(x), data_params)

y_pred_test = clf.predict(data_params)

data_params_test_correct = [data_params[i] for i in range(len(data_params))  if  y_pred_test[i] == -1]
data_params_test_incorrect = [data_params[i] for i in range(len(data_params))if   y_pred_test[i] == 1]






## plot Age vs Fare using the following metric
## ['Pclass','Sex','Embarked','Age','Fare']
## for non-Survived data
ages_correct=np.array(data_params_test_correct)[:,3]
fares_correct=np.array(data_params_test_correct)[:,4]
ages_incorrect=np.array(data_params_test_incorrect)[:,3]
fares_incorrect=np.array(data_params_test_incorrect)[:,4]
b_correct = pl.scatter(ages_correct,fares_correct, c='green')
b_incorrect = pl.scatter(ages_incorrect,fares_incorrect, c='red')
n_correct = ages_correct.size
n_error = ages_incorrect.size
pl.xlim((ages_correct.min(),ages_correct.max() ))
pl.ylim((fares_correct.min(),fares_correct.max() ))
pl.xlabel('ages')
pl.ylabel('fares')
pl.legend([ b_correct,b_incorrect],
           ["correct (%d)"%n_correct, "incorrect (%d)"%n_error],
           loc="upper left",
           prop=matplotlib.font_manager.FontProperties(size=11))

pl.title("Test on non-Survived data (%d)"%len(data_params))
pl.show()



# 8) make predictions on the enumerated test dataset one-class SVM
df = pd.read_csv("test.csv")
# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

data = df[cols_to_keep[1:]]
data_params = data[cols_to_keep[1:]].as_matrix()
data_params=list(data_params)
data_params=map(lambda x: list(x), data_params)

predicted = clf.predict(data_params)


survived = [data_params[i] for i in range(len(data_params)) if predicted[i] == 1]
nosurvived = [data_params[i] for i in range(len(data_params)) if predicted[i] ==  -1]



## plot Age vs Fare using the following metric
## ['Pclass','Sex','Embarked','Age','Fare']
## for non-Survived data
ages_surv=np.array(survived)[:,3]
fares_surv=np.array(survived)[:,4]
ages_nosurv=np.array(nosurvived)[:,3]
fares_nosurv=np.array(nosurvived)[:,4]
b_surv = pl.scatter(ages_surv,fares_surv, c='green')
b_nosurv = pl.scatter(ages_nosurv,fares_nosurv, c='red')
n_correct = ages_surv.size
n_error = ages_nosurv.size
pl.xlim((ages_surv.min(),ages_surv.max() ))
pl.ylim((fares_surv.min(),fares_surv.max() ))
pl.xlabel('ages')
pl.ylabel('fares')
pl.legend([ b_surv,b_nosurv],
           ["survived (%d)"%n_correct, "nosurvived (%d)"%n_error],
           loc="upper left",
           prop=matplotlib.font_manager.FontProperties(size=11))

pl.title("Test on test.csv (%d)"%len(data_params))
pl.show()


raw_input("Press the Enter key...")

