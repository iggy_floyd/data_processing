import numpy as np
from sklearn import preprocessing



'''
	 simple dataset with 3 classes: "1","2","3"
         each class is given by a 4-dimensional vector with components ranged between 0 and 9

'''
X = np.array([['Class 1',0,1,2,3],['Class 2',4,5,6,7],['Class 3',8,9,7,2]])

def load_digits(X):
 ''' split dataset into data and labels
     return a data matrix where a 4-dimensional state vector is a column 
 '''
 
 y = X[:,0] # to keep labels
 X = X[:,1:]
 return (np.transpose(X).astype("float32"),y)


def scale(X,eps=0.001):
 ''' scale the data points s.t the columns of the feature space
     (i.e the predictors) are within the range [0, 1]
 '''
 return (X - np.min(X, axis = 0)) / (np.max(X, axis = 0) + eps)


## test 
X,y = load_digits(X)
print X
print y
print scale(X)


'''
 a dataset of several categorial variables X0,Y0

'''
X0 = [1,3,5,2,3,8,9,2,0,4,7,6] # some value from 0..9
Y0 = [1,0,1,2,1,1,0,2,1,2,0,1] # some value from 0..2


dataset = zip(X0,Y0)
dataset = map(lambda x: list(x),dataset)

# test
enc = preprocessing.OneHotEncoder()
print dataset
#enc.fit([[1, 1], [3, 0], [5, 1], [2, 2], [3, 1], [8, 1], [9, 0], [2, 2], [0, 1], [4, 2], [7, 0], [6, 1]])
enc.fit(dataset)
print '(2,0):', enc.transform([[2,0]]).toarray()
print '(0,1):', enc.transform([[0,1]]).toarray()
print '(8,2):', enc.transform([[8,2]]).toarray()
