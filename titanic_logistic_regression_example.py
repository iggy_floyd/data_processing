''' This is an example of the application of the Logistic fucntion to the dataset analysis. 
    Logistic function is helpful to model binary models

    This is a simple and naive analysis w/o dedicated sklearn usage.

    The basic idea is taken from 
    http://blog.yhathq.com/posts/logistic-regression-and-python.html
    http://www.ats.ucla.edu/stat/r/dae/logit.htm
    

    About Logistic function one can read at 
    http://en.wikipedia.org/wiki/Logit



    DataFrameImputer was taken from 
    http://stackoverflow.com/questions/25239958/impute-categorical-missing-values-in-scikit-learn

'''

import pandas as pd
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import sys
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO
import prettytable    
import statsmodels.api as sm
import pylab as pl 


class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)

def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out


def isolate_and_plot(combos,value_name,variable_name,group_name):
 #isolate_and_plot(combos,'Survived_pred','Age','Pclass')

# print 'combos'
# print combos

 grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name, group_name], aggfunc=np.mean)
 # grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name, group_name])
# grouped2 = pd.pivot_table(combos, values=[value_name], cols=[variable_name], rows=[ group_name], aggfunc=np.mean)

# print 'grouped data'
# print grouped2.head()

# return 

 colors = 'rbgyrbgy'
 for col in combos[group_name].unique():
 # print 'col',col
#  print grouped.index.get_level_values(1)
  plt_data = grouped.ix[grouped.index.get_level_values(1)==col]
#  print 'ptl_data'
#  print plt_data
  pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],
  color=colors[int(col)])
 
 pl.xlabel(variable_name)
 pl.ylabel("P(%s)"%value_name)
 pl.legend([str(i) for i in range(int(combos[group_name].max()))], loc='upper left', title=group_name)
 pl.title("Prob(%s) isolating %s variable and group %s"%(value_name,variable_name,group_name))
 pl.show() 



def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]   
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders 


# 1) get data for an analysis
df = pd.read_csv("train.csv")

# 2) take a look at the dataset
print '**'*60,'\n'
print df.head()
print '**'*60,'\n'

# 3) summarize data
print '**'*60,'\n'
print df.describe()
print '**'*60,'\n'


# 4) print cross table
crosstabe=['Sex','Age']
print '**'*60,'\n'
print pd.crosstab(df[crosstabe[0]], df[crosstabe[1]], rownames=[crosstabe[0]])
print '**'*60,'\n'

# 5) plot some distributions
df.hist()
pl.show()

# 6) we are going to analyze a regressiong factors between 'Survied' as a target and Pclass,Sex,Age,Fare,Embarked
# also we introduce dummy categories for all of the Pclass,Sex,Embarked  to do this analysis.
input_variable_names = ['Pclass','Sex','Embarked','Age','Fare']

# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# get classification labels
labels = df["Survived"].values

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

# plot again  some distributions
df.hist()
pl.show()

# add dummy columns of categorial variables to the dataset:
# for Pclass: 3
dummy_pclass_ranks = pd.get_dummies(df['Pclass'], prefix='Pclass')
print '**'*60,'\n'
print dummy_pclass_ranks.head()
print '**'*60,'\n'
# create a clean data frame for the regression
cols_to_keep = ['Survived']+input_variable_names[1:]
data = df[cols_to_keep].join(dummy_pclass_ranks.ix[:, 'Pclass_1':]) 

# add dummy columns of categorial variables to the dataset:
# for Sex: 2
dummy_sex_ranks = pd.get_dummies(data['Sex'], prefix='Sex')
cols_to_keep = ['Survived']+input_variable_names[2:] + ['Pclass_1','Pclass_2','Pclass_3']
data = data[cols_to_keep].join(dummy_sex_ranks.ix[:, 'Sex_1':])

# add dummy columns of categorial variables to the dataset:
# for Embarked: 3
dummy_emb_ranks = pd.get_dummies(data['Embarked'], prefix='Embarked')
cols_to_keep = ['Survived']+input_variable_names[3:] + ['Pclass_1','Pclass_2','Pclass_3'] + ['Sex_1']
data = data[cols_to_keep].join(dummy_emb_ranks.ix[:, 'Embarked_1':])

# manually add the intercept needed to the Logist function
data['intercept'] = 1.0 

print '**'*60,'\n'
print data.head()
print '**'*60,'\n'

# 7) do a regression analysis
train_cols = ['Pclass_1','Pclass_2','Pclass_3','Sex_1','Embarked_1','Embarked_2','Embarked_3','intercept'] # MLE is not converged
# remove baselines from the fit
train_cols = ['Pclass_2','Pclass_3','Sex_1','Embarked_2','Embarked_3','intercept'] # MLE is converged

logit = sm.Logit(data['Survived'], data[train_cols])

# fit a model
result = logit.fit() 
print result.summary() 

# 7.2) make predictions on the enumerated dataset
data['Survived_pred'] = result.predict(data[train_cols])
print 'data predict'
print data.head()



# 8) you can estimate odds and their ratios
print '**'*60,'\n'
print np.exp(result.params)
print '**'*60,'\n'

print '**'*60,'\n'
print 'Ratio Pclass_2 to Pclass_3'
print np.exp(result.params['Pclass_2']-result.params['Pclass_3'])
print '**'*60,'\n'


# 9) Finall analysis:
# As a way of evaluating our classifier, 
# we're going to recreate the dataset with every logical combination of input values. 
# This will allow us to see how the predicted probability of admission increases/decreases across different variables.


# We are going to analyze Probability to Survice as  functions of Age and Fare for the  'Pclass' and 'Sex' categories

# instead of generating all possible values of Age  and Fare, we're going
# to use an evenly spaced range of 10 values from the min to the max
ages = np.linspace(data['Age'].min(), data['Age'].max(), 10)
fares = np.linspace(data['Fare'].min(), data['Fare'].max(), 10)


# enumerate all possibilities for 'Pclass',Sex and Embarked categories
combos = pd.DataFrame(cartesian([ages, fares, [1, 2, 3],[0,1],[1,2,3],[1.]]))   
combos.columns = ['Age', 'Fare', 'Pclass', 'Sex_1','Embarked' ,'intercept']

dummy_ranks_pclass = pd.get_dummies(combos['Pclass'], prefix='Pclass')
dummy_ranks_pclass.columns = ['Pclass_1', 'Pclass_2', 'Pclass_3']

dummy_ranks_emb = pd.get_dummies(combos['Embarked'], prefix='Embarked')
dummy_ranks_emb.columns = ['Embarked_1', 'Embarked_2', 'Embarked_3']




# keep only what we need for making predictions
cols_to_keep = ['Age', 'Fare','Pclass','Embarked','Sex_1', 'intercept']# ,'Pclass_2','Pclass_3','Embarked_2','Embarked_3'] 
combos = combos[cols_to_keep].join(dummy_ranks_pclass.ix[:, 'Pclass_2':])
cols_to_keep = ['Age', 'Fare','Pclass','Embarked','Sex_1', 'intercept' ,'Pclass_2','Pclass_3'] #,'Embarked_2','Embarked_3']
combos = combos[cols_to_keep].join(dummy_ranks_emb.ix[:, 'Embarked_2':])

# make predictions on the enumerated dataset
combos['Survived_pred'] = result.predict(combos[train_cols]) 
print combos.head()

isolate_and_plot(combos,'Survived_pred','Age','Pclass')
isolate_and_plot(combos,'Survived_pred','Fare','Sex_1')


raw_input("Press the key...")






